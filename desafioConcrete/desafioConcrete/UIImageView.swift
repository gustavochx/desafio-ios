//
//  UIImageView.swift
//  DesafioConcrete
//
//  Created by Gustavo Henrique on 25/12/16.
//  Copyright © 2016 Gustavo Henrique. All rights reserved.
//

import UIKit

extension UIImageView  {
    
    func roundToCircle(){
        self.layer.borderWidth = 1.0
        self.layer.cornerRadius = self.bounds.width/2
        self.layer.masksToBounds = true
    }
    
}
