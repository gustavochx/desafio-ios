//
//  UIViewController.swift
//  DesafioConcrete
//
//  Created by Gustavo Henrique on 26/12/16.
//  Copyright © 2016 Gustavo Henrique. All rights reserved.
//

import UIKit

extension UIViewController {

    func setAlert(msg: String, completion: (() -> ())?) {
        
        let alertController = UIAlertController(title: "Mensagem", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            completion?()
        }
        
        alertController.view.layer.cornerRadius = 15
        alertController.view.tintColor = UIColor.black
        alertController.view.backgroundColor = UIColor().UIColorFromRGB(rgbValue: 0x1ABC9C)
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func activityView(view: UIView, overlayView: UIView, activityView: UIActivityIndicatorView) {
        
        
        overlayView.frame = view.bounds
        overlayView.center = CGPoint(x: view.bounds.width / 2.0, y:view.bounds.height / 2.0)
        overlayView.backgroundColor = UIColor.white
        overlayView.alpha = 0.8
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        
        activityView.frame = CGRect(origin: CGPoint(x:0, y:0), size: CGSize(width:40, height:40))
        activityView.activityIndicatorViewStyle = .gray
        activityView.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        
        overlayView.addSubview(activityView)
        view.addSubview(overlayView)
        
        activityView.startAnimating()

    }
    
}
