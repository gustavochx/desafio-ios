//
//  RepositoryPersistenceTests.swift
//  DesafioConcrete
//
//  Created by Gustavo Henrique on 26/12/16.
//  Copyright © 2016 Gustavo Henrique. All rights reserved.
//

import XCTest

@testable import DesafioConcrete

class RepositoryPersistenceTests: XCTestCase {
    
    
    func testingGetRepositories() {
        
        var repositories = [Repository]()
        let expec = expectation(description: "Request return")
        
        RepositoryHandler.repositoriesFrom(page: "1"){ (result: [Repository]) in
            repositories = result
            
            expec.fulfill()
        }
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(repositories.count > 0)
        }
        
    }
    
    
}
