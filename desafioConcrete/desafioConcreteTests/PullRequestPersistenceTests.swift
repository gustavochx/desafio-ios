//
//  PullRequestPersistenceTests.swift
//  DesafioConcrete
//
//  Created by Gustavo Henrique on 26/12/16.
//  Copyright © 2016 Gustavo Henrique. All rights reserved.
//

import XCTest

@testable import DesafioConcrete

class PullRequestPersistenceTests: XCTestCase {
    
    func testingGetRequests() {
        
        var requests = [PullRequest]()
        let expec = expectation(description: "Request return")
        
        PullRequestHandler.requestsFrom(login: "elastic", name: "elasticsearch"){ (result: [PullRequest]) in
            requests = result
            expec.fulfill()
        }
        
        waitForExpectations(timeout: 10.0) { (error) in
            XCTAssert(requests.count > 0)
        }
    }
    
}
